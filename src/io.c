/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/time.h>
#include <inttypes.h>
#include <string.h>

#include <fcntl.h>

#include "io.h"
#include "list.h"
#include "log.h"
#include "timeout.h"

/*
typedef struct io_handlers_s {
    io_callback_f read_cb;
    io_callback_f write_cb;
} io_handlers_t;
*/

/*
typedef struct io_config_s {
    io_handlers_t cb;
    struct timeval tout;
} io_config_t;
*/

typedef struct io_item_s {
    int fd;
    io_handlers_t *h;
    struct timespec *timeout;
    struct timespec time_left;
    void *data;
} io_item_t;

static void add_fd_to_sets(fd_set *rset, fd_set *wset);

static list_t *io_list = NULL;
static int max_fd = 0;

int io_init(void)
{
    io_list = list_create();
    if( io_list != NULL )
        return 0;
    else
        return -1;
}

void io_deinit(void)
{
    list_foreach( io_list )
    {
        free( el->item );
    }
    list_destroy( io_list );
}

int io_add_watch(int fd, io_config_t *cfg)
{
    return io_add_watch_data( fd, cfg, NULL );
}

int  io_add_watch_data(int fd, io_config_t *cfg, void *data)
{
    int ret;

    if( cfg == NULL )
    {
        log_error( "io_add_watch: cfg is NULL" );
        return -1;
    }

    io_item_t *tmp = malloc( sizeof(io_item_t) );
    if( tmp == NULL )
    {
        log_error( "io_add_watch: malloc failed" );
        return -2;
    }

    tmp->fd = fd;
    tmp->h = &cfg->cb;
    tmp->timeout = &cfg->timeout;
    tmp->data = data ? data : NULL;
    memcpy( &tmp->time_left, tmp->timeout, sizeof( struct timespec ) );

    if( fd > max_fd )
        max_fd = fd;

    ret = list_insert( io_list, tmp );

    if( ret == 0 )
    {
        log_debug( "io_add_watch: success" );
    }
    else
        log_error( "io_add_watch: failed with error code: %d", ret );

    return ret;
}

int io_del_watch(int fd)
{
    list_foreach(io_list)
    {
        if( el->item && ((io_item_t *)el->item)->fd == fd )
        {
            free( (io_item_t *)el->item );
            list_delete(io_list, el);
            break;
        }
    }
    log_debug( "io_del_watch: success" );
    return 0;
}

int io_del_watch_data(void *data)
{
    list_foreach(io_list)
    {
        if( el->item && ((io_item_t *)el->item)->data == data )
        {
            free( (io_item_t *)el->item );
            list_delete(io_list, el);
        }
    }
    log_debug( "io_del_watch: success" );
    return 0;
}


int io_watch( void )
{
    int ret = 0;
    io_item_t *tmp_item = NULL;

    struct timespec *timeout;
    uint8_t timeout_malloced = 0;
    
    time_measure_t time_measure;

    fd_set rdset;
    fd_set wrset;

    FD_ZERO( &rdset );
    FD_ZERO( &wrset );

    add_fd_to_sets( &rdset, &wrset);
    
    if( list_is_empty( io_list ) == 0 ) 
        timeout = timeout_find_smallest_timeout( io_list, STRUCT_OFFSET(io_item_t, timeout), STRUCT_OFFSET(io_item_t, time_left) );
    else
    {
        timeout = malloc( sizeof( struct timespec ) );
        if( timeout == NULL )
        {
            log_error( "io_watch(): out of memory" );
            return -1;
        }
        timeout_malloced = 1;
        timeout->tv_sec = 1;
        timeout->tv_nsec = 0;
    }

    //log_debug( "io_watch: ready to call select, max_fd: %d", max_fd );
    if( timeout_measure_time( TIME_MEASURE_START, &time_measure ) != 0 )
    {
        log_error( "Unable to start time measurement" );
    }
    
    struct timeval select_timeout;
    TIMESPEC_TO_TIMEVAL( &select_timeout, timeout );
    //log_debug( "calling select with timeout: sec %d, usec: %d", select_timeout.tv_sec, select_timeout.tv_usec );
    ret = select( max_fd + 1, &rdset, &wrset, NULL, &select_timeout );
    if( timeout_measure_time( TIME_MEASURE_STOP, &time_measure ) != 0 )
    {
        log_error( "Failed to stop time measurement" );
    }
    if( timeout_malloced )
       free( timeout );
    
    //log_debug( "io_watch: select returned with: %d", ret );
    
    if( ret > 0 )
    {
        int rd_ret = 0;
        int wr_ret = 0;

        list_foreach(io_list)
        {
            tmp_item = (io_item_t *)el->item;
            if( FD_ISSET( tmp_item->fd, &rdset ) && tmp_item->h->read_cb )
            {
                //log_debug( "io_watch: read ready on fd: %d", tmp_item->fd );
                rd_ret = tmp_item->h->read_cb(tmp_item->fd, tmp_item->data );
                memcpy( &tmp_item->time_left, tmp_item->timeout, sizeof( struct timespec ) );
            }
            else
            {
                timeout_update_time_left( &tmp_item->time_left, &time_measure.time_diff );
            }
            if( FD_ISSET( tmp_item->fd, &wrset ) && tmp_item->h->write_cb )
            {
                //log_debug( "io_watch: write ready on fd: %d", tmp_item->fd );
                wr_ret = tmp_item->h->write_cb(tmp_item->fd, tmp_item->data );
            }
            if( wr_ret || rd_ret )
            {
                log_error( "io_watch: read or write callback returned error", tmp_item->fd );
            }
        }
    }
    else if( ret == 0 )
    {
        io_item_t *tmp = NULL;
        if(errno == 0)
        {
            //log_debug( "io_watch: select timeout" );
            list_foreach(io_list)
            {
                tmp = (io_item_t *)el->item;
                timeout_update_time_left( &tmp->time_left, &time_measure.time_diff );
            }
        }
    }
    else
    {
        log_error( "io_watch: errno = %s.", strerror(errno));
    }
    
    tmp_item = NULL;
    list_foreach( io_list )
    {
        tmp_item = (io_item_t *)el->item;
        if( timeout_check_for_timeout( &tmp_item->time_left ) )
        {
            //log_debug( "io_watch: timeout on fd: %d", tmp_item->fd );
            memcpy( &tmp_item->time_left, tmp_item->timeout, sizeof( struct timespec ) );
            if( tmp_item->h->timeout_cb )
            {
                if( tmp_item->h->timeout_cb(tmp_item->fd, tmp_item->data) )
                {
                    log_error( "io_watch: timeout callback returned error", tmp_item->fd );
                }
            }
        }
    }
    return ret;
}

static void add_fd_to_sets(fd_set *rset, fd_set *wset)
{
    list_foreach(io_list)
    {
        //log_debug( "add_fd_to_sets" );
        
        if( fcntl( ((io_item_t *)el->item)->fd, F_GETFL ) < 0 && errno == EBADF )
            log_error( "%s: file descriptor is not valid: %d", ((io_item_t *)el->item)->fd );

        if( ((io_item_t *)el->item)->h->read_cb )
        {
            FD_SET( ((io_item_t *)el->item)->fd, rset );
        }

        if( ((io_item_t *)el->item)->h->write_cb )
        {
            FD_SET( ((io_item_t *)el->item)->fd, wset );
        }
    }
}
