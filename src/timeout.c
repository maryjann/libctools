/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <stdlib.h>
#include <string.h>

#include "log.h"
#include "io.h"
#include "list.h"
#include "timeout.h"

static inline unsigned long long timespec_to_nsec( struct timespec *in );
static inline void nsec_to_timespec( unsigned long long in, struct timespec *out );

struct timespec* timeout_find_smallest_timeout( list_t* list, size_t timeout_ptr_offset, size_t time_left_offset )
{
    struct timespec *timeout;
    struct timespec *time_left;
    struct timespec *prev_tout = NULL;
    
    list_foreach(list)
    {
        timeout = *((struct timespec **)GET_POINTER(el->item, timeout_ptr_offset));
        time_left = (struct timespec *)GET_POINTER(el->item, time_left_offset);

        if( prev_tout == NULL )
            prev_tout = timeout;
        else if( timeout )
        {
            if( timespec_to_nsec( prev_tout ) > timespec_to_nsec( time_left ) )
            {
                prev_tout = time_left;
            }
        }
    }

    return prev_tout;
}

int timeout_measure_time( time_measure_op_t op, time_measure_t *time )
{
    switch( op )
    {
        case TIME_MEASURE_START:
            if( clock_gettime( CLOCK_REALTIME, &time->time_diff ) != 0 )
            {
                log_error( "measure_time: failed to get system time" );
                time->status = TIME_MEASURE_STOP;
                return -1;
            }
            //else log_debug( "measure_time: got system time" );
            time->status = TIME_MEASURE_START;
            return 0;
        case TIME_MEASURE_STOP:
            if( time->status == TIME_MEASURE_STOP )
            {
                log_error( "measure_time: time measutre not started" );
                return -2;
            }
            else
            {
                struct timespec tmp_time;
                if( clock_gettime( CLOCK_REALTIME, &tmp_time ) != 0 )
                {
                    log_error( "measure_time: failed to get system time" );
                    time->status = TIME_MEASURE_STOP;
                    return -3;
                }
                nsec_to_timespec( timespec_to_nsec( &tmp_time ) - timespec_to_nsec( &time->time_diff ), &time->time_diff );
                time->status = TIME_MEASURE_STOP;
            }
            return 0;
    }
    return -4;
}

int timeout_update_time_left( struct timespec *time_left, struct timespec *time_measured )
{
    if( time_left->tv_sec < time_measured->tv_sec )
    {
        memset( time_left, 0, sizeof( struct timespec) );
        return 0;
    }
    time_left->tv_sec -= time_measured->tv_sec;
    
    if( time_left->tv_nsec < time_measured->tv_nsec )
    {
        if( time_left->tv_sec == 0 ) 
        {
            memset( time_left, 0, sizeof( struct timespec) );
            return 0;
        }
        time_left->tv_sec--;
        time_left->tv_nsec = 1000000000UL - time_measured->tv_nsec; 
    }
    else
        time_left->tv_nsec -= time_measured->tv_nsec;
    
    return 0;
}

uint8_t timeout_check_for_timeout( struct timespec *time_left )
{
    if( timespec_to_nsec( time_left ) == 0 ) {
        return 1;
    }
    return 0;
}

static inline unsigned long long timespec_to_nsec( struct timespec *in )
{
    unsigned long long ret = ( (unsigned long long)in->tv_sec * 1000000000ULL ) + in->tv_nsec;
    return ret;
}

static inline void nsec_to_timespec( unsigned long long in, struct timespec *out )
{
    out->tv_sec = in / 1000000000UL;
    out->tv_nsec = in % 1000000000UL;
}

