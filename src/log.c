/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <syslog.h>
#include <string.h>

#include "log.h"

static int severity = 0;

int log_init(char* log_prefix, int std_err, int log_severity)
{
    openlog( log_prefix, std_err ? LOG_PERROR : 0, LOG_USER );
    severity = log_severity;

    log_debug( "log opended" );

    return 0;
}

void log_debug(const char *format, ...)
{
    if( severity >= LOG_DEBUG )
    {
        char *tmp = malloc(strlen(format) + strlen("(debug): ") + 1 /* '\0' */ );
        sprintf( tmp, "%s%s", "(debug): ", format );
    
        va_list args;
        va_start(args, format);
        vsyslog( LOG_DEBUG, tmp, args );
        va_end( args );

        free(tmp);
    }
}

void log_error(const char *format, ...)
{
    char *tmp = malloc(strlen(format) + strlen("(error): ") + 1 /* '\0' */ );
    sprintf( tmp, "%s%s", "(error): ", format );

    va_list args;
    va_start(args, format);
    vsyslog( LOG_ERR, tmp, args );
    va_end( args );

    free(tmp);
}

void log_info(const char *format, ...)
{
    if( severity >= LOG_INFO )
    {
        char *tmp = malloc(strlen(format) + strlen("(info): ") + 1 /* '\0' */ );
        sprintf( tmp, "%s%s", "(info): ", format );

        va_list args;
        va_start(args,format);
        vsyslog( LOG_INFO, tmp, args );
        va_end( args );

        free(tmp);
    }
}

void log_close(void)
{
    closelog();
}
