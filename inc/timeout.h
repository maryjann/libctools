/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _TIMEOUT_H_
#define _TIMEOUT_H_

#include <time.h>
#include <inttypes.h>

#include "io.h"

#define TIMESPEC_TO_TIMEVAL(tv, ts) {                                   \
         (tv)->tv_sec = (ts)->tv_sec;                                    \
         (tv)->tv_usec = (ts)->tv_nsec / 1000;                           \
}

#define STRUCT_OFFSET(struct_name, field_name) \
    (size_t)(&(((struct_name *)0)->field_name))

#define GET_POINTER(base_address, offset) \
    ( (uint8_t *)base_address + offset )

typedef enum time_measure_op_e {
    TIME_MEASURE_STOP,
    TIME_MEASURE_START
} time_measure_op_t;

typedef struct time_measure_s {
    uint8_t status;
    struct timespec time_diff;
} time_measure_t;

struct timespec* timeout_find_smallest_timeout( list_t* list, size_t timeout_ptr_offset, size_t time_left_offset );
int timeout_measure_time( time_measure_op_t op, time_measure_t *time );
int timeout_update_time_left( struct timespec *time_left, struct timespec *time_measured );
uint8_t timeout_check_for_timeout( struct timespec *time_left );
#endif
