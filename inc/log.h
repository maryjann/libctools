/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _LOG_H_
#define _LOG_H_

#include <syslog.h>

/*
#define LOG(level, ...) \
    do {
        if (level >= g_level)
            log_error( __FUNCTION__"(): " __VA_ARGS__)
    } while (0)
*/

int log_init(char* log_prefix, int std_err, int log_severity);

void log_debug(const char *format, ...);
void log_error(const char *format, ...);
void log_info(const char *format, ...);

void log_close(void);

#endif //LOG_H
