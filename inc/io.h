/*
 * ----------------------------------------------------------------------------
 * "THE BEER-WARE LICENSE" (Revision 42):
 * <nowacki.jakub@gmail.com> wrote this file. As long as you retain this notice you
 * can do whatever you want with this stuff. If we meet some day, and you think
 * this stuff is worth it, you can buy me a beer in return Jakub Nowacki
 * ----------------------------------------------------------------------------
 */

#ifndef _IO_H_
#define _IO_H_

#include <time.h>

typedef int (*io_callback_f) (int fd, void *data);

typedef struct io_handlers_s {
    io_callback_f read_cb;
    io_callback_f write_cb;
    io_callback_f timeout_cb;
} io_handlers_t;

typedef struct io_config_s {
    io_handlers_t cb;
    struct timespec timeout;
} io_config_t;

int  io_init(void);
void io_deinit(void);

int io_add_watch(int fd, io_config_t *cfg);
int io_add_watch_data(int fd, io_config_t *cfg, void *data);

int io_del_watch(int fd);
int io_del_watch_data(void *data);

int  io_watch( void );

#endif /* IO_H */
